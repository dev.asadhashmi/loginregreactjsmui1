import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { userAuthApi } from '../services/userAuthApi'
import { userNewsApi } from '../services/userNewsApi'

import userReducer from '../features/userSlice'
import authReducer from '../features/authSlice'

export const store = configureStore({
  reducer: {
    [userNewsApi.reducerPath]: userNewsApi.reducer,
    [userAuthApi.reducerPath]: userAuthApi.reducer,
    user: userReducer,
    auth: authReducer,

  },
  middleware: (getDefaultMiddleware) =>
    // getDefaultMiddleware().concat(userNewsApi.middleware),
    getDefaultMiddleware().concat(userNewsApi.middleware,userAuthApi.middleware),

})
setupListeners(store.dispatch)