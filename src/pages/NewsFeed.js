import React from "react";
import laptop from "../images/laptop.jpg";
import { Image } from "@mui/icons-material";
import { AiFillClockCircle } from "react-icons/ai";

//
import { Button, CssBaseline, Grid, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import ChangePassword from './auth/ChangePassword';
import Navbar from '../components/Navbar';

import { useGetLoggedUserNewsQuery } from '../services/userNewsApi';

import { getToken, removeToken } from '../services/LocalStorageService';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setUserInfo, unsetUserInfo } from '../features/userSlice';
import { unsetUserToken } from '../features/authSlice';

//

const data = {
  articles: [
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Lauren Forristal",
      title: "YouTube reportedly experimenting with online games",
      description:
        "YouTube is reportedly testing an online game offering that could give users the ability to play games on desktop and mobile devices.",
      url: "https://techcrunch.com/2023/06/26/youtube-reportedly-experimenting-with-online-games/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2020/06/GettyImages-1149449078-e1610399732853.jpg?resize=1200,631",
      publishedAt: "2023-06-26T17:06:46Z",
      content:
        "Nearly six months after Google officially shut down its cloud gaming service Stadia, it appears the company may be launching a similar product through YouTube. The Google-owned video-sharing platform… [+1439 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Lorenzo Franceschi-Bicchierai",
      title: "Hacktivists steal government files from Texas city Fort Worth",
      description:
        "The city of Fort Worth admitted that it suffered a data breach. A hacktivist group claimed responsibility and leaked gigabytes of stolen data.",
      url: "https://techcrunch.com/2023/06/26/hacktivists-steal-government-files-from-texas-city-fort-worth/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2023/06/forth-worth-texas.jpg?resize=1200,800",
      publishedAt: "2023-06-26T16:34:24Z",
      content:
        "A hacktivist group broke into an online system belonging to the Texas city of Fort Worth, stole several gigabytes of data, and then posted it online.\r\nOn Saturday, Fort Worth officials admitted that … [+2955 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Alex Wilhelm",
      title:
        "Q2 failed to bring a funding reprieve for web3 startups and unicorns",
      description:
        "The gist is that the prevailing pessimism in the air is wearing down these once-key pillars of venture volume.",
      url: "https://techcrunch.com/2023/06/26/q2-failed-to-bring-a-funding-reprieve-for-web3-startups-and-unicorns/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2020/05/NSussman_Techcrunch_Exchange_v3-GRN.jpg?resize=1200,900",
      publishedAt: "2023-06-26T16:13:42Z",
      content:
        "Lo and behold, we’re already halfway into 2023, which means we’re only a couple weeks away from brand new, sizzling data on the second quarter. However, it’s always wise to keep an eye on the horizon… [+981 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Kyle Wiggers",
      title:
        "ThoughtSpot acquires Mode Analytics, a BI platform, for $200M in cash and stock",
      description:
        "ThoughtSpot announced that it's acquiring Mode Analytics, a business intelligence and analytics platform, for $200 million.",
      url: "https://techcrunch.com/2023/06/26/thoughtspot-acquires-mode-analytics-a-bi-platform-for-200m-in-cash-and-stock/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2022/11/GettyImages-1306800215.jpg?resize=1200,674",
      publishedAt: "2023-06-26T16:01:02Z",
      content:
        "ThoughtSpot, an AI-powered analytics startup, today announced that it’s entered into a definitive agreement to acquire Mode Analytics, a business intelligence platform, for $200 million in cash and s… [+1963 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Lauren Forristal",
      title: "Meta launches new VR subscription ‘Meta Quest+’ for $7.99",
      description:
        "Meta Quest+ is a new subscription that gives players access to the top two titles every month for $7.99.",
      url: "https://techcrunch.com/2023/06/26/meta-quest-launches-vr-subscription/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2022/07/GettyImages-1239009531.jpg?resize=1200,800",
      publishedAt: "2023-06-26T15:44:42Z",
      content:
        "Meta Quest users are getting a new subscription that gives them access to the top two titles every month. Dubbed Meta Quest+, the virtual reality subscription costs $7.99 per month or $59.99 annually… [+1180 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Mary Ann Azevedo",
      title:
        "As the generative AI craze rages on, Ramp acquires customer support startup Cohere.io",
      description:
        "Cohere.io had raised a total of $3.5 million in funding from investors such as Initialized Capital, Y Combinator and Ramp's co-founders.",
      url: "https://techcrunch.com/2023/06/26/as-the-generative-ai-craze-rages-on-fintech-ramp-acquires-ai-powered-customer-support-startup-cohere-io/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2023/04/GettyImages-1451031353.jpg?resize=1200,797",
      publishedAt: "2023-06-26T15:36:37Z",
      content:
        "Want more fintech news in your inbox? Sign up here.\r\nFinance automation company Ramp has acquired Cohere.io, a startup that built an AI-powered customer support tool, the companies told TechCrunch ex… [+5851 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Romain Dillet",
      title: "Amazon wants to turn small shops into delivery partners",
      description:
        "According to a new report from Axios, Amazon wants to partner with small businesses, such as bodegas and IT shops, so that they can become delivery partners. The company pays a small fee to small retailers for each package they deliver. With this program, Ama…",
      url: "https://techcrunch.com/2023/06/26/amazon-wants-to-turn-small-shops-into-delivery-partners/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2023/06/GettyImages-658510400.jpg?resize=1200,906",
      publishedAt: "2023-06-26T15:29:15Z",
      content:
        "According to a new report from Axios, Amazon wants to partner with small businesses, such as bodegas and IT shops, so that they can become delivery partners. The company pays a small fee to small ret… [+2517 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Ingrid Lunden",
      title: "Databricks picks up MosaicML, an OpenAI competitor, for $1.3B",
      description:
        "Investors aren’t the only ones who want to get their hands on hot tech companies in the field of AI: it’s also likely to spur a big wave of M&A, too: today, Databricks announced that it would pay $1.3 billion to acquire MosaicML, an open-source startup with n…",
      url: "https://techcrunch.com/2023/06/26/databricks-picks-up-mosaicml-an-openai-competitor-for-1-3b/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2023/06/GettyImages-1285877166.jpg?resize=1200,675",
      publishedAt: "2023-06-26T14:54:04Z",
      content:
        "Investors aren’t the only ones who want to get their hands on hot tech companies in the field of AI: it’s also likely to spur a big wave of M&amp;A, too: today, Databricks announced that it would pay… [+3980 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Ivan Mehta",
      title: "Netflix quietly axes its basic plan in Canada",
      description:
        "Netflix has quietly killed the CAD$9.99 per month basic plan in Canada for new subscribers, according to its support page.",
      url: "https://techcrunch.com/2023/06/26/netflix-quietly-axes-its-basic-plan-in-canada/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2023/02/GettyImages-1240099721.jpeg?resize=1200,800",
      publishedAt: "2023-06-26T14:09:01Z",
      content:
        "Netflix has quietly killed the $9.99 CAD per month basic plan in Canada for new subscribers, as first noted by the Canadian publication BlogTo. This simplifies the streaming company’s offering but le… [+1832 chars]",
    },
    {
      source: {
        id: "techcrunch",
        name: "TechCrunch",
      },
      author: "Lauren Simonds",
      title:
        "Glean operational advice from Airbnb and Instacart at TC Disrupt 2023’s Builders Stage",
      description:
        "Airbnb’s Naba Banerjee and Instacart’s Asha Sharma discuss operations and the art of scaling at the TechCrunch Disrupt 2023 Builders Stage.",
      url: "https://techcrunch.com/2023/06/26/airbnb-instacart-operations-scale-builder-stage-techcrunch-disrupt-2023/",
      urlToImage:
        "https://techcrunch.com/wp-content/uploads/2023/06/tc_disrupt_2023_speaker-carousel_1920x1080_Naba-BanerjeeAsha-Sharma.png?resize=1200,675",
      publishedAt: "2023-06-23T17:13:53Z",
      content:
        "What does it take to turn an unknown startup into a household name? The ability to scale. Its the Holy Grail that every early-stage startup founder chases. Scaling internal operations may lack the gl… [+3732 chars]",
    },
  ],
};

const NewsFeed = () => {

  const [userData, setUserData] = useState({
    articles: ""
  })
  const navigate = useNavigate()
  const token = getToken()
  const newsData = useGetLoggedUserNewsQuery(token)

  return (
    <div className="container">
      {newsData.currentData.apiData.map((article, index) => (
        <div className="news-card" key={index}>
          <div className="img-fluid">
            <img src={article.imageURL} alt="Article" />
          </div>
          <div className="d-flex fs-12" style={{ gap: "6px" }}>
            <div className=" chip">id#{article.sourceId}</div>
            <div className=" chip">{article.sourceName}</div>
            <div className="chip">
              <AiFillClockCircle color="	#818589" />
              {article.publishedAt}
            </div>
          </div>

          <div className="">
            <div className="fw-700 fs-20">{article.author}</div>
            <div className="fs-14" style={{ color: "#4a4949" }}>
              {article.title}
            </div>
          </div>
          <div className="fs-14">
            <div className="" style={{ paddingBottom: "5px" }}>
              {article.description}
            </div>
          </div>
          <div className="card-content fs-14">{article.description}</div>
        </div>
      ))}
    </div>
  );
};

export default NewsFeed;
